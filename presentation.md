---
title: Presentation for COMPSCI 203
author: Xing Shi Cai
date: 2021-09-17
geometry:
- margin=1.5in
...

As part of the evaluation process,
you are required to give a 15-minutes presentation,
on a *problem* (or *problems*) in discrete mathematics,
either remotely or in person (as you wish).
Your presentation will be graded and be part of your final score.

[This YouTube video](https://youtu.be/eIb1cz06UwI) is a good example of what is
expected from your presentation.

# The Topic

You can choose a problem of your like, as long as 

1. it belongs to logic, combinatorics, graph theory, or discrete probability theory;
2. we have **not** covered the topic at the time of your presentation.
3. it is suitable for an audience consisting of students who are taking this course.

Some books from which you may find a suitable problem ---

1. [Discrete Mathematics --- An Open Introduction, 3rd edition](https://discrete.openmathbooks.org/) (our textbook)
2. [Applied Combinatorics](https://www.rellek.net/book/app-comb.html) (our textbook)
3. [Concrete Mathematics: A Foundation for Computer Science](https://www-cs-faculty.stanford.edu/~knuth/gkp.html)
4. [Combinatorics Through Guided Discovery](https://bogart.openmathbooks.org/ctgd/ctgd.html)
5. [Graph Theory with Applications by Bondy and Murty](http://www.maths.lse.ac.uk/Personal/jozef/LTCC/Graph_Theory_Bondy_Murty.pdf)

You can also choose other sources such as a blog, a paper, a Youtube video, etc.

Here are some example of suitable topics ---

* Some examples of where Catalan numbers appear
* A graph with $n$ vertices and $m$ edges has at least $m-n+1$ cycles.
* Giving a combinatorial proof of an identity
* The non-attacking rooks problem

You don't need to email me your topic beforehand. But if you do, I will give you some advice.

# The Time Limit

You should pick a problem that is simple enough so it can be explained in 15 minutes.
But you also should prepare enough material to more or less use up your time.

# The Format

You can use slides, write on a (electronic) white board, or combine different
presentation methods and technologies.

# The Schedule

The presentation will be done in the 5, 6, and 7 weeks of the course.
Students will be allocated randomly in one of the three weeks.
If you have *strong* preference for when to do your presentation,
please contact me *before* week 5.
