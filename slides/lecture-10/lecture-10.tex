%\documentclass[draft]{beamer}
\documentclass{beamer}

\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 10 --- Inclusion-Exclusion (2)}

\begin{document}

\maketitle

\section{Derangement}

\begin{frame}
    \frametitle{Swedish graduations}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./cap.jpg}
        \caption*{It is a tradition for students in Sweden to wear a white cap on their
        graduation day.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{A cap puzzle}
    
    🤔️ Suppose that $20$ Swedish students 
    get very excited in their graduation ceremony and throw their caps into the air. 
    Each student gets a cap back uniformly at random.
    What is the probability that no one gets his/her own cap back?
\end{frame}

\begin{frame}[t]
    \frametitle{Derangement}
    
    Fix a positive integer $n$, let $X$ denote the set of all permutations on
    $[n]$. 

    A permutation $\sigma \in X$ is called a \alert{derangement} if $\sigma(i) \ne i$ for all
    $i \in [n]$.

    🤔️ Which of the two following permutations are derangements?
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./derangement.png}
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{Count $N(S)$}
    
    Let $P_{i}$ denote that $\sigma(i) = i$.

    🤔️ If $n=5$, what is $N(\{1\})$, $N(\{1,2\})$, $N(\{1,2,3\})$?

    \practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{Count $N(S)$}
    
    \begin{exampleblock}{Lemma 7.10. (AC)}
    For each subset $S \subseteq [n]$ with $|S|=k$, we have
        \begin{equation*}
            N(S) = (n − k)!
        \end{equation*}
    \end{exampleblock}
\end{frame}

\begin{frame}[t]
    \frametitle{Count derangements}
    \begin{exampleblock}{Theorem 7.11 (AC)}
        For each positive integer $n$,
        the number $d_n$ of derangements of $[n]$ satisfies
        \begin{equation*}
            d_n=\sum_{k=0}^n (-1)^k\binom{n}{k}(n-k)!.
        \end{equation*} 
    \end{exampleblock}
\end{frame}

\begin{frame}[t]
    \frametitle{The cap puzzle}
    
    🤔️ Suppose that $20$ Swedish students 
    get very excited in their graduation ceremony and throw their caps into the air. 
    Each student gets a cap back uniformly at random.
    What is the probability that no one get his/her own cap back?

    The answer is
    \begin{equation*}
        \frac{d_{20}}{20!} = \frac{4282366656425369}{11640679464960000} \approx 0.36787944117144232161
    \end{equation*}

    What is this \href{https://www.wolframalpha.com/input/?i=0.36787944117144232161}{number}?
\end{frame}


\begin{frame}
    \frametitle{When $n$ grows large}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./derangement-1.png}
        \caption*{$\log(d_{n}/n! - e^{-1})$}%
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{The limit}

    \begin{exampleblock}{Theorem 7.12}
    For a positive integer $n$, let $d_n$ denote the number of derangements of $[n]$.
    Then 
    \begin{equation*}
        \lim_{n \to \infty} \frac{d_{n}}{n!} = \frac{1}{e}.
    \end{equation*}
    \end{exampleblock}
\end{frame}

 
\section{The Euler Totient Function}

\begin{frame}[t]
    \frametitle{The Euler $\phi$/totient function}
    
    For a positive integer n ≥ 2, let
    \begin{equation*}
        \phi(n) = |\{m \in \dsZ_{+} : m \le n, \gcd(m, n) = 1\}|.
    \end{equation*}
    
    🤔️ What is $\phi(5), \phi(9)$?

    \practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{Count multipliers}

    $12$ has two prime factors $p_{1} = 2, p_{2} = 3$.

    How many in $[12]$ are multiples of $2$, $3$ and $2 \times 3$ respectively?

    \practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{Count multipliers}

    \begin{exampleblock}{Proposition 7.15}
        Let $n \ge 2, k \ge 1$, and let $p_1 , p_2 , \ldots , p_k$ 
        be distinct primes each of which divide $n$ evenly (without remainder). 
        Then the number of integers from $\{1, 2, \ldots, n\}$
        which are divisible by each of these k primes is
        \begin{equation*}
            \frac{n}{p_{1}p_{2}\ldots p_{n}}
        \end{equation*}
    \end{exampleblock}

    Can you give a proof?

    \practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{Compute $\phi(n)$}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.3\linewidth]{./venn-3.png}
    \end{figure}

    $30$ has 3 \emph{distinct} prime factors $2, 3, 5$.

    Let $P_{1}, P_{2}, P_{3}$ be the properties of being a multiple of $2, 3, 5$ respectively.

    Can you use inclusion-exclusion to compute $\phi(30)$?

    \practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{A theorem for $\phi(n)$}

    \begin{block}{Theorem 7.14.}
    Let $n \ge 2$ be a positive integer and suppose that $n$ has $m$ distinct prime factors:
    $p_1 , p_2 , \ldots, p_m$. 
    Then
    \begin{equation*}
        \phi(n)= n \prod_{i=1}^{m} \frac{p_{i}-1}{p_{i}}
    \end{equation*}
    \end{block}
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Study guide}

    \begin{columns}[totalwidth=\textwidth]
        \exercisecolumn{}

        \begin{column}{0.5\textwidth}
            You are recommended to
            \begin{itemize}
                \item 📖 Read
                    \href{https://www.rellek.net/book/app-comb.html}{Applied
                    Combinatorics}, chapter 7.4-7.5.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
