%\documentclass[draft]{beamer}
\documentclass{beamer}

\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 04 -- Proof techniques}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{A classic theorem}
    
    \begin{theorem}
        There are infinitely many primes.
    \end{theorem}

    People use 🤖️ to find larger and larger primes.

    The largest known prime now is $2^{82,589,933} - 1$.

    However, only a \emph{proof} can convince us the theorem is true.
\end{frame}

\begin{frame}
    \frametitle{Proof by contradiction}

    \begin{proof}
        \scriptsize
        \begin{enumerate}[{P}1]
            \item Suppose there are only finitely many primes. (Premise)
            \item Let $p$ be the largest prime. (by P1)
            \item Let $N = p! + 1$. (Just a notation)
            \item $N > p$. (by P3)
            \item $N$ is not divisible by any $i \in \{1,\dots, p\}$. (by P3)
            \item $N$ has a prime factor greater than $p$. (by P5)
            \item $p$ is not the largest prime. (by P6)
            \item P7 contradicts P2.
            \item P1 must be false.
            \item There must be infinitely many primes. (by P9)
        \end{enumerate}
    \end{proof}
\end{frame}

\section{Direct Proof}

\begin{frame}
    \frametitle{The recipe of direct proof}
    
    The general format to prove $P \imp Q$ is
    \begin{itemize}
        \item Assume $P$. Explain, explain, \dots, explain. Therefore $Q$.
    \end{itemize}

    Often we want to prove 
    $$\forall x (P(x) \imp Q(x)).$$
    In this case, we fix $x$ to be an \emph{arbitrary} element of the things we are
    interested in.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.3}

    Prove: For all integers $a$, $b$, and $c$, if $a|b$ and $b|c$ then $a|c$. 
\end{frame}

\section{Proof by Contrapositive}

\begin{frame}
    \frametitle{The recipe of proof by contrapositive}

    Recall that $P \imp Q$ is logically equivalent to $\neg Q \imp \neg P$.
    
    So we can prove the latter instead using this format
    \begin{itemize}
        \item Assume $\neg Q$. Explain, explain, \dots explain. Therefore $\neg P$.
    \end{itemize}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.4}
    
    Prove: If $n^2$ is even, then $n$ is even.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.5}
    
    Prove: For all integers $a$ and $b$, if $a + b$ is odd, then $a$ is odd or $b$ is odd.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.6}
    
    Prove: For every prime number $p$, either $p=2$ or $p$ is odd.
\end{frame}

\section{Proof by Contradiction}

\begin{frame}
    \frametitle{The recipe of proof by contradiction}

    A \alert{contradiction} is a statement of the form $P \wedge \neg P$. It is
    \emph{always} false.

    Let $C$ be a contradiction. The statement
    \begin{equation*}
        (\neg X \imp C) \imp X,
    \end{equation*}
    is always true.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./contradiction.png}
    \end{figure}

    So if we can show that $\neg X$ implies a contradiction, then $X$ must be true.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.7}
    
    Prove that $\sqrt{2}$ is irrational.
\end{frame}

\begin{frame}
    \frametitle{Did Hippasus drown because of $\sqrt{2}$?}
    
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \small

            Hippasus of Metapontum (530--450 BC) was a Pythagorean philosopher. 

            \smallskip

            He is credited with the discovery of the existence of irrational numbers. 

            \smallskip

            The discovery of irrational numbers is said to have been shocking to the
            Pythagoreans, and Hippasus is supposed to have drowned at sea, 
            as a punishment from the gods.

            \smallskip

            \hfill --- From \href{https://en.wikipedia.org/wiki/Hippasus}{Wikipedia}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{./Hippasus.png}
                \caption*{Hippasus.  By \href{https://books.google.co.uk/books?id=1urLxOK8pzkC}{Girolamo Olgiati}}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.8}
    
    Prove: There are no integers $x$ and $y$ such that $x^2 = 4y + 2$.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.9}

    The Pigeon-hole Principle: If more than $n$ 🐦️ fly into $n$ pigeon
    holes, then at least one pigeon hole will contain at least two 🐦️.
\end{frame}

\section{Proof by (Counter) Example}

\begin{frame}
    \frametitle{An experiment}
    
    Consider $n^2 - n + 41$ for the first few $n \in \dsN$.
 
    \begin{equation*}
        \begin{array}{ c c c c c c c c  }
            \toprule
            n & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\
            \midrule
            n^2 − n + 41 & 41 & 43 & 47 & 53 & 61 & 71 & 83 \\
            \bottomrule
        \end{array}
    \end{equation*}
    we have gotten only primes. 

    Is it true that
    \begin{itemize}
        \item For all $n \in \dsN$, $n^{2} - n + 41$ is prime.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{You cannot prove by example}
    
    💣️ It is almost never OK to prove $\forall x P(x)$ by examples.

    To prove that
    \begin{itemize}
        \item For all $n \in \dsN$, $n^{2} - n + 41$ is prime.
    \end{itemize}
    it is not enough to show it true for $n \in \{1, \dots, 7\}$.
\end{frame}

\begin{frame}
    \frametitle{You can prove by example}
    But it is fine to use example to show $\exists x P(x)$ by one example.

    To prove that
    \begin{itemize}
        \item There exists $n \in \dsN$, $n^{2} - n + 41$ is \emph{not} a prime.
    \end{itemize}
    it is not enough to show it is true for $n=41$.
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.10}
    
    We proved, ``for all integers $a$ and $b$, if $a + b$ is odd, then $a$ is odd or
    $b$ is odd.'' Is the converse true?
\end{frame}

\begin{frame}
    \frametitle{Strong law of small numbers}
    
    There aren't enough small numbers to meet the many demands made of them.

    \hfill --- Richard K. Guy 

    
    \begin{exampleblock}{A more striking example}
        The statement
        \begin{equation*}
            \ceil*{ \frac{2}{2^{1/n}-1}} = \floor*{ \frac{2 n}{\log(2)} }
        \end{equation*}
        is true for $n$ from $1$ to $777451915729367$ but false for $777451915729368$
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{A different opinion}

    Unfortunately, these examples serve as `weapons' in the propaganda efforts of
    rigorists, fanatical, `purists', who see truth as black and white, and claim that
    you can't guarantee the truth of a statement no matter how many special cases you
    have checked.
    
    \hfill --- \emph{Why the Cautionary Tales Supplied by Richard Guy's Strong Law of Small Numbers
    Should not be Overstated}, By Doron Zeilberger
\end{frame}

\section{Proof by Cases}

\begin{frame}
    \frametitle{The recipe of proof by cases}
    
Suppose at least one of the statements $Q_1, Q_2 , \dots , Q_n$ is true. 

If we can show that $Q_1 \imp P$ and $Q_2 \imp P$ and \dots $Q_n \imp P$, then $P$
must be true.
    
Example ---
We want to show
\begin{enumerate}
    \item If $x$ is either 🐶️, 😺️, 🐷️, then we like $x$.
\end{enumerate}
The proof can be
\begin{enumerate}
    \item 
    If $x$ is a 🐶️, then we like $x$.
    If $x$ is a 😺️, then we like $x$.
    If $x$ is a 🐷️, then we like $x$.
\end{enumerate}
\end{frame}

\begin{frame}[t]
    \frametitle{Example 3.2.11}

    Prove: For any integer $n$, the number $(n^3 − n)$ is even.
\end{frame}

\begin{frame}[t]
    \frametitle{Strangers and friends}
    
    \begin{theorem}
        In any group of 6 people, there are either 3 mutual friends (every pair knows each other) or 3
        mutual strangers (no pair knows each other).
    \end{theorem}
\end{frame}

\section{Is this a correct proof?}

\begin{frame}
    \frametitle{Is this a correct proof?}

    Prove: If $ab$ is an even number, then $a$ or $b$ is even.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./proof1.png}
    \end{figure}
    \practice{}
\end{frame}

\begin{frame}
    \frametitle{Is this a correct proof?}

    Prove: If $ab$ is an even number, then $a$ or $b$ is even.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./proof2.png}
    \end{figure}
    \practice{}
\end{frame}

\begin{frame}
    \frametitle{Is this a correct proof?}

    Prove: If $ab$ is an even number, then $a$ or $b$ is even.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./proof3.png}
    \end{figure}
    \practice{}
\end{frame}

\begin{frame}
    \frametitle{Is this a correct proof?}

    Prove: If $ab$ is an even number, then $a$ or $b$ is even.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./proof4.png}
    \end{figure}
    \practice{}
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Study guide}

    \begin{columns}[totalwidth=\textwidth]
        \exercisecolumn{}

        \begin{column}{0.5\textwidth}
            You are recommended to
            \begin{itemize}
                \item 📖 Read
                    \href{http://discrete.openmathbooks.org/dmoi3/sec_intro-statements.html}{Discrete
                    Mathematics}, chapter 3.2.
                \item 📖 Read
                    \href{https://en.wikipedia.org/wiki/Ramsey\%27s_theorem\#R(3,_3)_=_6}{Wikipedia} for the example of ``Strangers and Friends''.
                \item 🖊 Solve
                    \href{http://discrete.openmathbooks.org/dmoi3/sec_intro-statements.html}{Discrete
                    Mathematics}, exercise 3.2: 1-7, 9, 10, 13
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\story{}

\begin{frame}
    \frametitle{Alan Turing}

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\linewidth]{./Turing.jpg}
            \caption*{Statue of Turing. Photo by \href{https://commons.wikimedia.org/w/index.php?curid=20247421}{Antoine Taveneaux}, CC BY-SA 3.0}%
        \end{figure}
        \end{column}
        \begin{column}{0.5\textwidth}
        \begin{figure}[htpb]
            \centering
            \includegraphics[width=0.8\linewidth]{./the-imitation-game-movie-poster.jpg}
            \caption*{Turing played by Benedict Cumberbatch in \href{https://en.wikipedia.org/wiki/The_Imitation_Game}{The Imitation Game}}%
        \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Turing's life}

    Alan Turing (23 June 1912 – 7 June 1954) was an English mathematician, 
    computer scientist, logician, cryptanalyst.  

    His achievements include

    \begin{itemize}
        \item Turing machine.
        \item Halting problem.
        \item Breaking Nazi German cyphers in WWII.
        \item Designing one of the early computers.
        \item Turing test.
        \item Wrote a software to generate random love letters.
        \item \dots
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Something we can learn from Turing}

    Perhaps this was the most surprising thing about Alan Turing.
    Despite all he had done in the war, 
    and all the struggles with stupidity,
    he still did \emph{not} think of intellectuals or scientists as forming a
    superior class.

    \hfill --- Andrew Hodges, \emph{Alan Turing: The Enigma}
\end{frame}


\end{document}
