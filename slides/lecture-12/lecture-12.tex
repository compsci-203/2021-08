%\documentclass[draft]{beamer}
\documentclass{beamer}

\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 12 --- Generating Functions (2)}


\begin{document}

\maketitle

\section{Making a Fruit Basket}

\begin{frame}
    \frametitle{Example 8.5}

    A grocery store is preparing holiday fruit baskets for sale. 

    Each fruit basket will have $n$ pieces of fruit in it, chosen from 🍎, 🍌, 🍊️, and 🍐️.

    How many different ways can such a basket be prepared if 
    \begin{itemize}
        \item there must be at least one 🍎 in a basket, 
        \item a basket cannot contain more than three 🍌,
        \item and the number of 🍊️ must be a multiple of four.
    \end{itemize}

\end{frame}

\begin{frame}[t]
    \frametitle{GF for 🍎}

    Let $a_{n}$ be the number of ways to put in $n$ 🍎 in the basket so that we have at least one 🍎.  

    The GF for $(a_{n})_{n \ge 0}$ is
    \begin{equation*}
        A(x) = x + x^{2} + x^{3} + \cdots = \frac{x}{1-x}
    \end{equation*}
\end{frame}


\begin{frame}[t]
    \frametitle{GF for 🍌}

    Let $b_{n}$ be the number of ways to put in $n$ 🍌 in the basket so that we have at most three 🍌. 

    The GF for $(b_{n})_{n \ge 0}$ is
    \begin{equation*}
        B(x) = 1 + x + x^{2} + x^{3}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{GF for 🍊️}

    Let $c_{n}$ be the number of ways to put in $n$ 🍊️ in the basket so that we have a multiple of four 🍊️. 

    The GF for $(c_{n})_{n \ge 0}$ is
    \begin{equation*}
        C(x) = 1 + x^{4} + x^{8} + \cdots = \frac{1}{1-x^{4}}
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{GF for 🍐️}

    Let $d_{n}$ be the number ways to put in $n$ 🍐️ in the basket.

    The GF for $(d_{n})_{n \ge 0}$ is
        \begin{equation*}
            D(x) = 1 + x + x^{2} + \cdots = \frac{1}{1-x}
        \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{The GF for fruit baskets}

    Let $e_{n}$ be the number of ways to put $n$ fruits in the basket so \emph{all}
    conditions are satisfied.

    The GF for $(e_{n})_{n \ge 0}$ is
    \begin{equation*}
        \begin{aligned}
            \sum_{n = 0}^{\infty} e_{n} x^{n} 
            &
            =
            A(x) B(x) C(x) D(x)
            \\
            &
            =
            \frac{x}{1-x}
            (1 + x + x^{2} + x^{3})
            \frac{1}{1-x^{4}}
            \frac{1}{1-x}
            \\
            &
            =
            \sum_{n = 0}
            \binom{n+1}{2}
            x^{n}
        \end{aligned}
    \end{equation*}

    The last step follows either by either by 💪️ or by
    \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5Bx\%2F\%281-x\%29*\%281\%2Bx\%2Bx\%5E2\%2Bx\%5E3\%29\%281\%2F\%281-x\%5E4\%29\%29\%281\%2F\%281-x\%29\%29\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{WolframAlpha}.
\end{frame}

\begin{frame}
    \frametitle{The Recipe 🍜️ --- Integer composition with GF}

    The fruit basket problem is like asking the number of integer solutions of
    \begin{equation*}
        x_{1} + x_{2} + \cdots x_{k} = n
    \end{equation*}
    such that $x_{1},\ldots, x_{k}$ satisfies some restrictions.

    We can solve such a problem by
    \begin{itemize}
        \item find the GF for each variable (fruit) according to \emph{restrictions};
        \item multiply them together to a new GF\@;
        \item extract the coefficients of the new GF, either by 💪️ or by 🤖️;
        \item this gives the answer.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Try yourself}
    
    Consider the equality
    \begin{equation*}
        x_1 + x_2 + x_{3} = n
    \end{equation*}
    where $x_1, x_{2}, x_3, n \ge 0$ are all integers. 

    Suppose also that $x_1 \ge 2$, $x_2$ is a multiple of 5, and $0 \le x_3 \le 4$. 

    Let $c_n$ be the number of solutions.

    Use GF to find a closed formula for $c_n$.

    \poll{}
\end{frame}

\section{Newton's Binomial Theorem}

\begin{frame}[t]
    \frametitle{Extend the definition of \(\binom{n}{m}\)}
    For integers $n\ge m\ge0$, 
    \[
        \binom{n}{m}
        =
        \frac{n!}{m! (n-m)!}
        =
        \frac{n(n-1)(n-2)\dots(n-m+1)}{m!}
    \]

    We extend this by letting \(r \in \mathbb R\) and $k \in \dsZ$, and define
    \[
        \binom{r}{k}
        =
        \begin{cases}
            \frac{r(r-1)(r-2)\dots(r-k+1)}{k!} & k > 0, \\
            1 & k = 0, \\
            0 & k < 0.
        \end{cases}
    \]

    🤔️ What are $\binom{\pi}{-1}$, $\binom{\pi}{0}$ and $\binom{-1/2}{2}$?
\end{frame}

\begin{frame}[t]{Binomial Theorem}
    \begin{block}{Theorem 2.30 (AC)}
        For all \(p \in \mathbb Z\) with \(p \ge 0\),
        \[
            (1+x)^{p} = \sum_{n = 0}^{p} \binom{p}{n} x^{n}
        \]
    \end{block}
\end{frame}

\begin{frame}[t]{Newton's Binomial Theorem}
    \begin{block}{Theorem 8.10 (AC)}
        For all \(p \in \mathbb R\) with \(p \ne 0\),
        \[
            (1+x)^{p} = \sum_{n = 0}^{\infty} \binom{p}{n} x^{n}
        \]
    \end{block}

    For a proof, see
    \href{https://math.libretexts.org/Bookshelves/Combinatorics_and_Discrete_Mathematics/Combinatorics_and_Graph_Theory_\%28Guichard\%29/03\%3A_Generating_Functions/3.02\%3A_Newton\%27s_Binomial_Theorem}{here}.
    
    🤔️ Why is this the same as Binomial Theorem when $p \in \dsN$?
\end{frame}

\begin{frame}[t]{Applying Newton's Binomial Theorem}
    \begin{block}{Lemma 8.12 (AC)}
        For all integers \(m \ge 0\), 
        \vspace{-1em}
        \[
            \binom{-1/2}{m}=(-1)^{m} \frac{\binom{2m}{m}}{2^{2m}}. 
        \]
        \vspace{-1em}
    \end{block}
    \begin{block}{Theorem 8.13 (AC)}
        \[
            \frac{1}{\sqrt{1-4x}}
            =
            \sum_{n \ge 0} \binom{2n}{n} x^n
            .
        \]
        \vspace{-1em}
    \end{block}
\end{frame}

\begin{frame}[t]{An identity}
    \begin{block}{Corollary 8.14 (AC)}
    For all integers \(n \ge 0\)
    \begin{equation*}
        2^{2n} = \sum_{k \ge 0} \binom{2k}{k} \binom{2n-2k}{n-k}.
    \end{equation*}
    \vspace{-2em}
    \end{block}

    💡️ Can you prove this using
    \begin{equation*}
        \left(\sum_{n = 0}^{\infty} a_{n} x^{n}\right)
        \left(\sum_{n = 0}^{\infty} b_{n} x^{n}\right) 
        = 
        \sum_{n=0}^{\infty} \left(\sum_{k=0}^{n} a_{k} b_{n-k}\right) x^{n}
        .
    \end{equation*}
    and
    \[
        \frac{1}{\sqrt{1-4x}}
        =
        \sum_{n \ge 0} \binom{2n}{n} x^n
    \]

    \practice{}
\end{frame}

\section{Appendix}

\appendix{}

\begin{frame}
    \frametitle{Study guide}

    \begin{columns}[totalwidth=\textwidth]
        \exercisecolumn{}

        \begin{column}{0.5\textwidth}
            You are recommended to
            \begin{itemize}
                \item 📖 Read
                    \href{https://www.rellek.net/book/app-comb.html}{Applied
                    Combinatorics}, chapter 8.2-8.4.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
