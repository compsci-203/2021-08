%\documentclass[draft]{beamer}
\documentclass{beamer}

\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 09 --- Inclusion-Exclusion}

\begin{document}

\maketitle

\section{Introduction}

\begin{frame}
    \frametitle{The number of CS students}
    
    Let $X$ be the set of $63$ students in a discrete math course. 

    Suppose there are $47$ computer science majors and $51$ 👨️ students. 

    Also, we know there are $45$ 👨️ students majoring in computer science. 

    🤔️ How many students in the class are 👩️ students \emph{not} majoring in computer science?
\end{frame}

\begin{frame}
    \frametitle{A Venn Diagram}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./cs-student.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Amanda's money again}
    
    Amanda has \$100 which she wants to distribute among her four children, such
    that the third child has at most \$7 and the fourth has at most \$8.
    How many ways can she do that?

    This is to ask for the number integer solutions of
    \begin{equation*}
        x_1 + x_2 + x_3 + x_4 = 100
    \end{equation*}
    with $x_{1}, x_{2}, x_{3}, x_{4} \ge 0$, $x_{3} \le 7$ and $x_{4} \le 8$.

    \poll{}
\end{frame}

\section{The Inclusion-Exclusion Formula}

\begin{frame}
    \frametitle{Notations}
    
    Let $X$ be a set and let $\scP = \{P_1 , P_2 , \ldots, P_m \}$ be a family of properties. 

    For a subset $S \subseteq [m]$, let $N(S)$ denote the number of elements of $X$
    which satisfy property $P_i$ for all $i \in S$. 

    💣️ If $S = \emptyset$, then $N(S) = |X|$.
\end{frame}

\begin{frame}
    \frametitle{The CS student example}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{./cs-student.png}
    \end{figure}

    Let $P_{1}$ be \emph{male} and $P_{2}$ be \emph{CS Major}.

    Then we were computing
    \begin{equation*}
        N(\emptyset) - N(\{1\}) - N(\{2\}) + N(\{1, 2\}).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Amanda's example}
    
    What is the number integer solutions of
    \begin{equation*}
        x_1 + x_2 + x_3 + x_4 = 100
    \end{equation*}
    with $x_{1}, x_{2}, x_{3}, x_{4} \ge 0$, $x_{3} \le 7$ and $x_{4} \le 8$?

    Let $P_{1}$ be \emph{$x_{3} > 7$} and $P_{2}$ be \emph{$x_{4} > 8$}.

    Then the answer is
    \begin{equation*}
        N(\emptyset) - N(\{1\}) - N(\{2\}) + N(\{1, 2\}).
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Three properties}
    
    Let $P_{1}, P_{2}, P_{3}$ be having a 🐶️, a 😺️, a 🐷️ respectively.

    The number of students having none of these pets is
    \begin{align*}
        &
        N(\emptyset) 
        \\
        & 
        - N(\{1\}) - N(\{2\}) - N(\{3\})
        \\
        & 
        + N(\{1, 2\}) + N(\{2, 3\}) + N(\{3, 1\})
        \\
        & 
        -
        N(\{1, 2, 3\})
        .
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Three properties --- Venn Diagram}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{./venn-3.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Four properties}
    
    Let $P_{1}, P_{2}, P_{3}, P_{4}$ be having a 🐶️, a 😺️, a 🐷️, a 🐊 respectively.

    The number of students having none of these pets is
    \begin{align*}
        &
        N(\emptyset) 
        \\
        & 
        - N(\{1\}) - N(\{2\}) - N(\{3\}) - N(\{4\})
        \\
        & 
        + N(\{1, 2\}) + N(\{2, 3\}) + N(\{3, 4\}) + N(\{4, 1\}) + N(\{4, 2\}) + N(\{1, 3\})
        \\
        & 
        - N(\{1, 2, 3\}) - N(\{2, 3, 4\}) - N(\{3, 4, 1\}) - N(\{4, 1, 2\})
        \\
        &
        +
        N(\{1, 2, 3, 4\})
        .
    \end{align*}
\end{frame}

\begin{frame}
    \frametitle{Four properties --- Venn Diagram}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{./venn-4.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Principle of inclusion-exclusion}

    \begin{block}{Theorem 7.7 AC}
        The number of elements of $X$ which satisfy none of the properties in $\scP =
        \{P_{1}, P_{2}, \ldots, P_{m}\}$ is given by
    \begin{equation*}
        \sum_{S \subseteq [m]} (−1)^{|S|} N(S).  \tag{7.2.1}
    \end{equation*}
    \end{block}
\end{frame}

\section{Enumerating Surjections}

\begin{frame}
    \frametitle{Surjective (onto) functions}

    When a function's range contains the codomain, we
    say that is a \alert{surjective/onto function} or a \alert{surjection}.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./surjective.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Lottery tickets}
    
    A grandfather has 15 \emph{distinct} lottery tickets and wants to distribute them to his
    five grandchildren so that each child gets at least one ticket.  In how many ways
    can he make such a distribution?

    This is to ask what is the number of surjections from $[15]$ to $[5]$.
    
    💣️ The problem is different to distributing 15 1-dollar coins.
\end{frame}

\begin{frame}
    \frametitle{Notations}
    
    Let $S(n, m)$ be the number of surjections from $[n]$ to $[m]$.

    Let $X$ be the set of all functions from $[n]$ to $[m]$.

    $f \in X$ satisfies $P_{i}$ if $i$ is \emph{not} in the range of $f$.

    💣️ $S(n, m) = 0$ for all $n < m$.
\end{frame}

\begin{frame}
    \frametitle{Do you understand the notations?}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./surjections.png}
    \end{figure}

    \practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{$N(S)$}
    
    \begin{block}{Lemma 7.8 (AC)}
        For each $S \subseteq [m]$ with $\abs{S} = k$,
        \begin{equation*}
            N(S) = (m-k)^{n}.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}[t]
    \frametitle{The number of surjections}

    \begin{block}{Theorem 7.9}
        The number $S(n, m)$ of surjections from $[n]$ to $[m]$ is given by:
        \begin{equation*}
            S(n,m)
            =
            \sum_{k=0}^{m} (-1)^{k} \binom{m}{k}(m-k)^{n}
            .
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Can you answer lottery question?}
    
    The first four terms of $(S(15,i))_{i \ge 1}$ are
    \begin{equation*}
        1, 32766, 14250606, 1016542800
    \end{equation*}
    Can you find $S(15, 5)$?
    Maybe 
    \href{https://oeis.org/search?q=1\%2C+32766\%2C+14250606\%2C+1016542800&sort=&language=english&go=Search}{OEIS}
    can help?

    What about $\left(\frac{S(15,i)}{i!}\right)_{i \ge 1}$?
    \begin{equation*}
        1, 16383, 2375101, 42355950
    \end{equation*}
    Check \href{https://oeis.org/search?q=1\%2C+16383\%2C+2375101\%2C+42355950&language=english&go=Search}{OEIS} again.
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Study guide}

    \begin{columns}[totalwidth=\textwidth]
        \exercisecolumn{}

        \begin{column}{0.5\textwidth}
            You are recommended to
            \begin{itemize}
                \item 📖 Read
                    \href{https://www.rellek.net/book/app-comb.html}{Applied
                    Combinatorics}, chapter 7.1-7.3.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
