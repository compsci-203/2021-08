%\documentclass[draft]{beamer}
\documentclass{beamer}

\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 03 -- Propositional Logic}

\begin{document}

\maketitle

\section{Introduction}

\begin{frame}
    \frametitle{Who gets a 🍪️?}
    Consider two arguments
    \[
        \begin{array}{ r l }
               & \text{If Edith eats her 🍎, then she can have a 🍪️.} \\
               & \text{Edith eats her 🍎.} \\
               \cline{2-2}
            \therefore & \text{Edith gets a 🍪️.}
        \end{array}
    \]
    and
    \[
        \begin{array}{ r l }
               & \text{Florence must eat her 🍎 in order to get a 🍪️.} \\
               & \text{Florence eats her 🍎.} \\
               \cline{2-2}
            \therefore & \text{Florence gets a 🍪️.}
        \end{array}
    \]
    Are these \alert{valid} arguments?

    \practice{}
\end{frame}

\begin{frame}
    \frametitle{Valid and invalid arguments}
    
    An \alert{argument} is a set of statements, one of which is called the
    \alert{conclusion} and the rest of which are called \alert{premises}. 

    An argument is \alert{valid} if the conclusion must be true whenever the
    premises are all true. 

    An argument is \alert{invalid} if it is
    \emph{possible} for all the premises to be true and the conclusion to be false.
\end{frame}

\begin{frame}
    \frametitle{Sound and unsound arguments}
    
    The following argument \emph{is} valid,
    \[
        \begin{array}{ r l }
               & \text{All 🗑️ are items made of gold.} \\
               & \text{All items made of gold are ⏰️-travel devices.} \\
               \cline{2-2}
            \therefore & \text{All 🗑️ are ⏰️-travel devices.}
        \end{array}
    \]
    But it is \alert{unsound}, because the premises are false.

    💣️ In mathematics, we only care about \emph{validity}, but in life you should also
    consider \emph{soundness}.
\end{frame}

\section{Truth Tables}

\begin{frame}
    \frametitle{Propositional logic}

    \begin{columns}
        \begin{column}{0.6\textwidth}
        A \alert{proposition} is simply a statement. 

        \alert{Propositional logic} studies when a statement is true.

        \begin{exampleblock}{The Game of Monopoly}
            If you get more doubles than any other player then you will lose,
            or if you lose then you must have bought the most properties.
        \end{exampleblock}

        When is this true?

        %This is to ask when is  $(P \imp Q) \wedge (Q \imp R)$ true.
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{./pexels-cottonbro-4004174.jpg}
                \caption*{By Cottonbro from Pexels}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Truth tables}
    
    In a \alert{truth table}, each row lists a possible combination of $T$'s and $F$'s, 
    and then marks down if a statement is true or false in this case.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./truth-table-1.png}
        \includegraphics[width=0.18\linewidth]{./truth-table-2.png}
        \caption*{Truth tables of logical connectives}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Example 3.1.1}
    
    The truth table of $\neg P \vee Q$ is
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.36\linewidth]{./truth-table-3.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The Monopoly problem}
    
    \begin{exampleblock}{The Game of Monopoly}
        If you get more doubles than any other player then you will lose,
        or if you lose then you must have bought the most properties.
    \end{exampleblock}

    This is simply $(P \imp Q) \vee (Q \imp R)$, whose truth table is

    \begin{figure}[htpb]
        \centering
        \only<1>{\includegraphics[width=0.8\linewidth]{./truth-table-4-0.png}}
        \only<2>{\includegraphics[width=0.8\linewidth]{./truth-table-4-1.png}}
    \end{figure}
\end{frame}

\section{Logical Equivalence}

\begin{frame}
    \frametitle{Logical equivalence}
    
    Two (molecular) statements $P$ and $Q$ are \alert{logically equivalent} if $P$ is
    true precisely when $Q$ is true.

    For example, $P \imp Q$ and $\neg P \vee Q$ are logically equivalent.
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{./truth-table-5.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Snow and rain}
    
    Are the statements
    \begin{itemize}
        \item it will not ☔️ or ❄️
        \item it will not ☔️ and it will not ❄️
    \end{itemize}
    logically equivalent?
    
    💡️ Filling in the following truth table ---
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{./truth-table-6.png}
    \end{figure}

    \poll{}
\end{frame}

\begin{frame}
    \frametitle{Boolean algebra}
    
    \begin{exampleblock}{De Morgan's Laws}
        \begin{itemize}
            \item $\neg (P \wedge Q) \iff \neg P \vee \neg Q$.
            \item $\neg (P \vee Q) \iff \neg P \wedge \neg Q$.
        \end{itemize}
    \end{exampleblock}

    \begin{exampleblock}{Implications are Disjunctions}
        \begin{itemize}
            \item $P \imp Q \iff \neg P \vee Q$.
        \end{itemize}
    \end{exampleblock}


    \begin{exampleblock}{Double Negation}
        \begin{itemize}
            \item $\neg \neg P \iff P$. (It is not the case that Sam is not a 🐶️ =
                Sam is a 🐶️)
        \end{itemize}
    \end{exampleblock}

    We can apply such logical equivalence to transform one statement into another.
\end{frame}

\begin{frame}[t]
    \frametitle{Negation of an Implication}

    Prove that the statements $\neg (P \imp Q)$ and $P \wedge \neg Q$ are logically
    equivalent.
\end{frame}

\begin{frame}
    \frametitle{Verifying equivalence}

    Are these two statements equivalent?
    \begin{itemize}
        \item $(P \vee Q) \imp R$
        \item $(P \imp R) \vee (Q \imp R)$
    \end{itemize}

    😥️ Transformation rules can prove equivalence, but \emph{cannot} disapprove it.
\end{frame}

\begin{frame}
    \frametitle{The advantage of truth table}
    
    However, truth tables can show that two statements
    are \emph{NOT} equivalent.
    
    \begin{exampleblock}{Two non-equivalent statements}
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./truth-table-7.png}
    \end{figure}
    \end{exampleblock}
\end{frame}

\section{Deductions}

\begin{frame}
    \frametitle{Deduction rule}
    
    \small
    The argument
    \[
        \begin{array}{ r l }
               & \text{If Edith eats her 🍎, then she can have a 🍪️.} \\
               & \text{Edith eats her 🍎.} \\
               \cline{2-2}
            \therefore & \text{Edith gets a 🍪️.}
        \end{array}
    \]
    has the form
    \[
        \small
        \begin{array}{ r l }
               & $P \imp Q$ \\
               & $P$ \\
               \cline{2-2}
            \therefore & $Q$
        \end{array}
    \]
    This is a \alert{deduction rule}, i.e., an argument form which is
    always \emph{valid}. 
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.25\linewidth]{./truth-table-8.png}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Deduction rule 1}
%    
%    \begin{columns}
%        \begin{column}{0.4\textwidth}
%    
%    If our axioms are
%    \begin{enumerate}
%        \item $P$
%        \item $P \imp Q$
%    \end{enumerate}
%    then $Q$ is true.
%            
%        \end{column}
%        \begin{column}{0.6\textwidth}
%            
%    If we our axioms are
%    \begin{enumerate}
%        \item Sam is a 🐶️.
%        \item If Sam is a 🐶️, then Sam is cute.
%    \end{enumerate}
%    Then \emph{Sam is cute} is true.
%        \end{column}
%    \end{columns}
%\end{frame}

\begin{frame}
    \frametitle{Deduction rule --- We like Sam}

    \begin{columns}
        \begin{column}{0.3\textwidth}
            Deduction rule ---
            \[
                \small
                \begin{array}{ r l }
               & $P \imp Q$ \\
               & $Q \imp R$ \\
               \cline{2-2}
                    \therefore & $P \imp R$
                \end{array}
            \]

        \end{column}
        \begin{column}{0.7\textwidth}
            Example ---
            \[
                \begin{array}{ r l }
               & \text{If Sam is a 🐶️, then Sam is cute.} \\
               & \text{If Sam is cute, then we like Sam.} \\
               \cline{2-2}
                    \therefore & \text{If Sam is a 🐶️, then we like Sam.}
                \end{array}
            \]
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Deduction rule --- Sam is not a 🐶️}
    \begin{columns}
        \begin{column}{0.3\textwidth}
            Deduction rule ---
            \[
                \small
                \begin{array}{ r l }
               & $P \imp Q$ \\
               \cline{2-2}
                    \therefore & $\neg Q \imp \neg P$
                \end{array}
            \]

        \end{column}
        \begin{column}{0.7\textwidth}
            Example ---
            \[
                \begin{array}{ r l }
               & \text{If Sam is a 🐶️, then Sam is cute.} \\
               \cline{2-2}
                    \therefore & \text{If Sam is \emph{not} cute, then Sam is \emph{not} a 🐶️.}
                \end{array}
            \]
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Deduction rule --- We always like Sam}
    \begin{columns}
        \begin{column}{0.3\textwidth}
            Deduction rule ---
            \[
                \small
                \begin{array}{ r l }
               & $P \imp Q$ \\
               & $\neg P \imp Q$ \\
               \cline{2-2}
                    \therefore & $Q$
                \end{array}
            \]

        \end{column}
        \begin{column}{0.7\textwidth}
            Example ---
            \[
                \begin{array}{ r l }
               & \text{\small If Sam is a 🐶️, then we like Sam.} \\
               & \text{\small If Sam is \emph{not} a 🐶️, then we like Sam.} \\
               \cline{2-2}
                    \therefore & \text{\small We like Sam}
                \end{array}
            \]
        \end{column}
    \end{columns}

    Show the above rule is valid by filling in the truth table.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.35\linewidth]{./truth-table-9.png}
    \end{figure}

    \practice{}
\end{frame}

\section{Beyond Propositions}

\begin{frame}
    \frametitle{Predicate Logic}
    
    We may want to prove a statement like
    \begin{itemize}
        \item All primes greater than 2 are odd.
    \end{itemize}
    This can be written as
    \begin{equation*}
        \forall x((P(x) \wedge x > 2) \imp O(x))
    \end{equation*}
    where $P(x)$ denotes $x$ is a prime and $O(x)$ denotes $x$ is odd.

    $P(x)$ and $O(x)$ are not propositions but \alert{predicates} 🔮️.

    The logic studying statements/propositions involving predicates is called \alert{predicate logic}.
\end{frame}

\begin{frame}
    \frametitle{Logical equivalence in predicate logic}
    
    There is no analogues to truth-tables for predicate logic.
    All we can do is to use transformation rules (logical equivalence).

    \begin{exampleblock}{Example of logical equivalence}
        ``There is no smallest number'' can be written as
        \begin{equation*}
            \neg \exists x \forall y (x \le y).
        \end{equation*}
        We can transform this into
        \begin{equation*}
            \forall x \exists y (y < x),
        \end{equation*}
        i.e.,
        ``for every $x$ there is a $y$ which is smaller than $x$''.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{What is a proof?}

    \begin{enumerate}
        \item We start with a set of statements known as \alert{axioms}. An axiom is
            a basic statement that is simply accepted as \emph{true}.
        \item We apply logical \emph{deduction rules} to the axioms, and the
            resulting statements, until we arrive at the \emph{statement} we wish to prove.
    \end{enumerate}

    Georg Cantor proposed a set-theory-based axiomatic system in the late 19th
    century.

    It has now been replaced by Zermelo-Fraenkel with Choice (ZFC) axioms.
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Study guide}

    \begin{columns}[totalwidth=\textwidth]
        \exercisecolumn{}

        \begin{column}{0.5\textwidth}
            You are recommended to
            \begin{itemize}
                \item 📖 Read
                    \href{http://discrete.openmathbooks.org/dmoi3/sec_intro-statements.html}{Discrete
                    Mathematics}, chapter 3.1.
                \item 🖊 Solve
                    \href{http://discrete.openmathbooks.org/dmoi3/sec_intro-statements.html}{Discrete
                    Mathematics}, chapter 3.1: exercise 1-6, 8, 10-12.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{Story Time (This will not appear in exams 😎️)}

\begin{frame}
    \frametitle{The drowning child thought experiment}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./drowning-child.jpg}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The drowning child -- explained}
    
    Philosopher \href{https://en.wikipedia.org/wiki/Peter_Singer}{Peter Singer} proposes a \href{https://youtu.be/gGczdp0SE0c}{thought experiment} ---

    \begin{quote}
        On your way to work, you pass a small pond. \dots  you are surprised to see a
        child splashing about in the pond. \dots  If you don't wade in and pull him out, he seems
        likely to drown. Wading in is easy and safe, but you will ruin the new shoes
        \dots  What should you do?
    \end{quote}
    
    Most people will say that we should save the child. 
    But Singer then asks why is that each year \emph{5.4 million children} die of
    preventable diseases worldwide, while most of us do nothing about it.
\end{frame}

\begin{frame}
    \frametitle{An argument for saving the child}

    In his book \href{https://www.thelifeyoucansave.org/the-book/}{The Life You Can Save}, Singer argues 
    \[
        \begin{array}{ r l }
               & \text{\small If there are ways to save children's lives which do not cost us much,}  \\
               & \quad \text{\small then we should help saving children's lives.} \\
               & \text{\small There are ways to save children's lives which do not cost us much.} \\
               \cline{2-2}
            \therefore & \text{\small We should help saving children's lives.}
        \end{array}
    \]
\end{frame}

\begin{frame}[label=current]
    \frametitle{What do you think?}
    
    \begin{columns}
        \begin{column}{0.5\textwidth}
            Is Singer's argument \emph{valid}? 

            Is it \emph{sound}? 

            What do you think? 

            And what will you \alert{do}?
        \end{column}
        \begin{column}{0.5\textwidth}
            
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{./the-life-you-can-save.jpg}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
