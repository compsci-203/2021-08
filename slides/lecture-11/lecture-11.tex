%\documentclass[draft]{beamer}
\documentclass{beamer}

\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 11 --- Generating Functions (1)}


\begin{document}

\maketitle

\section{Basic Notion and Terminology}

\begin{frame}{What is a GF (generating function)}
    Given an infinite sequence \(\sigma=(a_{0},a_{1},\dots)\), we associate it with a
    ``\emph{function}''
    \(F(x)\) written as 
    \[
        F(x)=\sum_{n \ge 0} a_{n} x^{n},
    \]
    called the \alert{generating function} of \(\sigma\).
        
    💣️ \(F(x)\) is \alert{not} actually a function and we will \alert{never} ask
    questions like ``what is $F(1/2)$?''
\end{frame}

\begin{frame}
    \frametitle{Examples of GF}

    What is the GF of $a_{0} = 1$, $a_{1} = -1$, and $a_{n} = 0$ for $n \ge 2$?

    \vspace{3em}
    
    What is the GF of $a_{n} = 1$ for all $n \ge 0$?

    \vspace{3em}
    
    What is the GF of $a_{n} = n!$ for all $n \ge 0$?

    \vspace{3em}
    
\end{frame}

\begin{frame}
    \frametitle{A quick exercise from calculus}

    What is
    \begin{equation*}
        2^{-0} + 2^{-1} + 2^{-2} + 2^{-3} + \cdots = \sum_{n = 0}^{\infty} \frac{1}{2^{n}}
    \end{equation*}
    and
    \begin{equation*}
        3^{-0} + 3^{-1} + 3^{-2} + 3^{-3} + \cdots = \sum_{n = 0}^{\infty} \frac{1}{3^{n}}
    \end{equation*}
    and
    \begin{equation*}
        2^{0} + 2^{2} + 2^{2} + 2^{3} + \cdots = \sum_{n = 0}^{\infty} 2^{n}
    \end{equation*}

    \practice{}
\end{frame}

\begin{frame}{Examples of GF}
    When $\abs{x} < 1$, we know that the sum converges and
    \[
        \sum_{n = 0}^{\infty} x^{n} = \frac{1}{1-x}.
    \]

    If \(a_{n}=1\) for all \(n \ge 0\), 
    then its generating function is
    \[
        F(x)=1 + x + x^{2} + x^{3} + \cdots = \sum_{n = 0}^{\infty} x^{n}.
    \]

    🤔️ Can we write
    \[
        F(x) = \frac{1}{1-x}
    \]
    What does it even mean?
\end{frame}

\begin{frame}
    \frametitle{Multiply polynomials}
    What is
    \begin{equation*}
        (1-x)(1+x)
    \end{equation*}
    and
    \begin{equation*}
        (1-x)(1+x+x^{2})
    \end{equation*}
    and
    \begin{equation*}
        (1-x)(1+x+x^{2}+x^{3})
    \end{equation*}

    What about
    \begin{equation*}
        (1-x)(1+x+x^{2}+x^{3}+\cdots)
    \end{equation*}
\end{frame}

\begin{frame}[t]
    \frametitle{Multiply GFs}
    
    Given two GFs $A(x) = \sum_{n=0}^{\infty} a_{n} x^{n}$ and $B(x) =
    \sum_{n=0}^{\infty} b_{n} x^{n}$,
    we \emph{define}
    \begin{equation*}
        A(x)B(x) = \sum_{n=0}^{\infty} \left(\sum_{k=0}^{n} a_{k} b_{n-k}\right) x^{n}
        .
    \end{equation*}

    💡️ Treat GFs as if they are polynomials.
\end{frame}

\begin{frame}[t]
    \frametitle{Try yourself}

    What is
    \begin{equation*}
        (1-x)^{2} (1 + 2 x + 3 x^{2} + 4 x^{3} + \cdots)
    \end{equation*}
    \practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{Add GFs}
    
    Given two GFs $A(x) = \sum_{n=0}^{\infty} a_{n} x^{n}$ and $B(x) =
    \sum_{n=0}^{\infty} b_{n} x^{n}$,
    we \emph{define}
    \begin{equation*}
        A(x)+ B(x) = \sum_{n=0}^{\infty} (a_{n} + b_{n}) x^{n}
        .
    \end{equation*}

    💡️ Treat GFs as if they are polynomials.
\end{frame}

\begin{frame}[t]
    \frametitle{Try yourself}

    What is
    \begin{equation*}
        (1 + x + x^{2} + x^{3} + x^{4} \cdots) + (1 - x + x^{2} - x^{3} + x^{4} \cdots)
    \end{equation*}
    as GFs?

    What is
    \begin{equation*}
        \frac{1}{1-x} + \frac{1}{1+x}
    \end{equation*}
    as functions?

    \practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{Derivative of GFs}
    The derivative $A(x) = \sum_{n=0}^{\infty} a_{n} x^{n}$ is
    \emph{defined} by
    \begin{equation*}
        \dv{x} A(x) = \sum_{n=0}^{\infty} n a_{n} x^{n-1}
        .
    \end{equation*}

    💡️ Treat GFs as if they are polynomials.
\end{frame}

\begin{frame}[t]
    \frametitle{Try yourself}

    What is 
    \begin{equation*}
        \dv{x} (1+x+x^{2}+x^{3}+\cdots)
    \end{equation*}
    as in GF?

    What is
    \begin{equation*}
        \dv{x} \frac{1}{1-x}
    \end{equation*}
    as in calculus?

    \practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{Integral of GFs}

    The integral of $A(x) = \sum_{n=0}^{\infty} a_{n} x^{n}$ is
    \emph{defined} by
    \begin{equation*}
        \int A(x) \dd x = \sum_{n=0}^{\infty} \frac{a_{n}}{n+1} x^{n+1}
        .
    \end{equation*}

    💡️ Treat GFs as if they are polynomials.
\end{frame}

\begin{frame}[t]
    \frametitle{Try yourself}

    What is
    \begin{equation*}
        \int (1-x+x^{2}-x^{3}+x^{4}+\cdots) \dd x
    \end{equation*}
    as GF?

    What is
    \begin{equation*}
        \int \frac{1}{1+x} \dd x
    \end{equation*}
    as in calculus?

    \practice{}
\end{frame}

\begin{frame}
    \frametitle{Shorthand for GFs}

    We write, as in calculus
    \begin{equation*}
        e^{x} = 1 + x + \frac{x^2}{2!} + \frac{x^{3}}{3!} + \cdots
    \end{equation*}
    and
    \begin{equation*}
        \log(1+x) = x - \frac{x^2}{2} + \frac{x^3}{2} - \frac{x^{4}}{4} + \cdots
    \end{equation*}
    💣️ But the LHS (functions) are simply a quick way to write the RHS (GFs).
\end{frame}

\begin{frame}
    \frametitle{Summary}
    
    To recap
    \begin{itemize}
        \item GFs are not functions.
        \item We have defined summation, multiplication, derivative and integral of
            GFs.
        \item 😎️ When doing computations,  we can treat GFs \emph{as if} they are
            polynomials or Taylor expansions for functions like $e^{x}$ and $\log(x)$.
    \end{itemize}
\end{frame}

\section{Amanda's Money Again}

\begin{frame}[t]
    \frametitle{Amanda's money again}
    Amanda's have $n$ one-dollar 💵️ which she wants to distribute to $5$ children
    so that each child gets at least one dollar. How many ways can she do that?

    The answer is
    \begin{equation*}
        \binom{n-1}{5-1}  = \binom{n-1}{4}
    \end{equation*}
    🤔️ Can we get this from GF?
\end{frame}

\begin{frame}[t]
    \frametitle{Just one child}
    
    Let $a_{n}$ be the number of ways to distribute $n$ dollars among \alert{one}
    child so that the child has $>0$ dollars.

    The GF of $(a_{n})_{n \ge 0}$ is
    \begin{equation*}
        \alert{0} x^{0} + x + x^{2} + x^{3} + x^{4} + \cdots = \frac{x}{1-x}
    \end{equation*}

\end{frame}

\begin{frame}[t]
    \frametitle{When there are five children}
    
    Let $b_{n}$ be the number of ways to distribute $n$ dollars among \alert{five}
    children  so that each child has $>0$ dollars.

    We know that 
    \begin{equation*}
        b_{n} = 
        \begin{cases}
            0 & (n < 5), \\
            {\displaystyle \binom{n-1}{4}} & (n \ge 5).
        \end{cases}
    \end{equation*}

    So the GF for $ (b_{n})_{n \ge 0}$ is
    \begin{equation*}
        \begin{aligned}
            \sum_{n = 0}^{\infty}
            b_{n} x^{n}
            &
            =
            0 x^{0}
            +
            0 x^{1}
            +
            0 x^{2}
            +
            0 x^{3}
            +
            0 x^{4}
            +
            \binom{4}{4} x^{5}
            +
            \binom{5}{4} x^{6}
            +
            \cdots
            \\
            &
            =
            x^{5}
            +
            5 x^{6}
            \cdots
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}
    \frametitle{Another way to get the GF}
    
    The GF of $(b_{n})_{n \ge 0}$ is \emph{also} the following
    \begin{equation*}
        {\color{red} (x + x^2 + \cdots)}
        {\color{blue} (x + x^2 + \cdots)}
        {\color{orange} (x + x^2 + \cdots)}
        {\color{webgreen} (x + x^2 + \cdots)}
        {\color{maroon} (x + x^2 + \cdots)}
    \end{equation*}

    💡️ Expanding the product, we get
    \begin{equation*}
        \begin{aligned}
        &
        0 x^{0}
        +
        0 x^{1}
        +
        0 x^{2}
        +
        0 x^{3}
        +
        0 x^{4}
        \\
        &
        +
        {\color{red}x}{\color{blue}x}{\color{orange}x}{\color{webgreen}x}{\color{maroon}x}
        \\
        &
        +
        (
        {\color{red}x}{\color{blue}x}{\color{orange}x}{\color{webgreen}x}{\color{maroon}x^{2}}
        +
        {\color{red}x}{\color{blue}x}{\color{orange}x}{\color{webgreen}x^{2}}{\color{maroon}x}
        +
        {\color{red}x}{\color{blue}x}{\color{orange}x^{2}}{\color{webgreen}x}{\color{maroon}x}
        +
        {\color{red}x}{\color{blue}x^{2}}{\color{orange}x}{\color{webgreen}x}{\color{maroon}x}
        +
        {\color{red}x^{2}}{\color{blue}x}{\color{orange}x}{\color{webgreen}x}{\color{maroon}x}
        )
        \\
        &
        +
        \sum_{\substack{k_{1}, \dots , k_{5} \ge 1\\ k_{1} + \dots k_{5} = 7}}
        {\color{red}x^{k_{1}}}{\color{blue}x^{k_{2}}}{\color{orange}x^{k_{3}}}{\color{webgreen}x^{k_{4}}}{\color{maroon}x^{k_{5}}}
        +
        \cdots
        \\
        &
        =
        x^{5}
        +
        5 x^{6}
        +
        \cdots
        \\
        &
        =
        \sum_{n = 0}^{\infty}
        b_{n} x^{n}
        \end{aligned}
    \end{equation*}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Simplify the GF}
    
    With a bit arithmetic, we can show that
    \begin{equation*}
        (x + x^2 + \cdots)^{5} 
        =
        \frac{x^{5}}{(1-x)^{5}}
        = 
        \frac{x^{5}}{4!} \times \dv[4]{}{x} 
        \left({\frac{1}{1-x}} \right)
        =
        \sum_{n = 0} \binom{n-1}{4} x^{n}
    \end{equation*}

    However, we can also put the following code in
    \href{https://www.wolframalpha.com/input/?i=SeriesCoefficient\%5Bx\%5E5\%2F\%281-x\%29\%5E5\%2C+\%7Bx\%2C+0\%2C+n\%7D\%5D}{WolframAlpha}
    \begin{verbatim}
        SeriesCoefficient[x^5/(1-x)^5, {x, 0, n}]
    \end{verbatim}
\end{frame}

\begin{frame}
    \frametitle{The Recipe 🍜️ --- Integer composition with GF}

    Amanda's-money type problem is like asking the number of integer solutions of
    \begin{equation*}
        x_{1} + x_{2} + \cdots x_{k} = n
    \end{equation*}
    such that $x_{1},\ldots, x_{k} \ge 1$.

    We can solve such a problem by
    \begin{itemize}
        \item find the GF for each child;
        \item multiply them together to a new GF\@;
        \item extract the coefficients of the new GF, either by 💪️ or by 🤖️;
        \item this gives the answer.
    \end{itemize}
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Study guide}

    \begin{columns}[totalwidth=\textwidth]
        \exercisecolumn{}

        \begin{column}{0.5\textwidth}
            You are recommended to
            \begin{itemize}
                \item 📖 Read
                    \href{https://www.rellek.net/book/app-comb.html}{Applied
                    Combinatorics}, chapter 8.1-8.2.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
