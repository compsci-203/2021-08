%\documentclass[draft]{beamer}
\documentclass[c]{beamer}

\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 28 --- Applying Probability to Combinatorics}

\begin{document}

\maketitle

 
\section{Applying Probability to Ramsey Theory}

\begin{frame}
    \frametitle{What is probabilistic method?}
    
    Let $X$ be the number of \pictext{iphone.png} of a (uniform) \emph{random} student.

    We only know that $E(X) = ❓️$ using the following poll.

    \poll{}

    Is there a student who does \emph{not} have an \pictext{iphone.png}?
\end{frame}

\begin{frame}[t]
    \frametitle{A lower bound of $R(n,n)$}
    
    \begin{block}{Theorem 11.4 (AC)}
    \begin{equation*}
        R(n,n) \ge \frac{n}{e \sqrt{2}}2^{\frac{n}{2}}.
    \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{A problem can get you and $A$ and a lot more \ldots}

    We have shown that for all $n$ large enough
    \begin{equation*}
        \frac{n}{e \sqrt{2}}2^{\frac{n}{2}} 
        \le 
        R(n,n)
        \le 
        \frac{2^{2n}}{4 \sqrt{\pi n}}.
    \end{equation*}

    \begin{block}{🤔️ Open questions}
    Is there a constant $c < 2$ such that for all $n$ large enough
    $R(n,n) < 2^{c n}$?

    Is there a constant $d > \frac{1}{2}$ such that for all $n$ large enough
    $R(n,n) > 2^{d n}$?
    \end{block}
\end{frame}

\section{Ramsey's Theorem}

\begin{frame}
    \frametitle{The party problem again}

    The statement that $R(3,3) = 6$ can be rephrased as follows --

    If we colour the edges of $K_{6}$ with two colours, there will always be a
    monochromatic triangle.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{./ramsey-party-colour.pdf}
        \caption{Can you find the monochromatic triangles?}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Ramsey's theorem for three colours}
    It is known that any edge-colouring of $K_{17}$ with three colours contains a 
    monochromatic triangle. But this is not true for $K_{16}$.

    So we say $R(3,3,3) = 17$.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.4\linewidth]{./K_16_partitioned_into_three_Clebsch_graphs.svg.png}
        \caption{An edge-colouring of $K_{16}$ without mono-triangle. %
            From \href{https://en.wikipedia.org/wiki/Ramsey\%27s_theorem}{Wikipedia}}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{R(3, 3, 4)}
    It is \href{https://link.springer.com/article/10.1007/s10601-016-9240-3}{known} 
    that any edge-colouring of $K_{30}$ with {\color{red}red}, {\color{blue}blue} and
    {\color{green}green} contains either 
    {a \color{red}red triangle}, or {a \color{blue}blue triangle} and
    {a \color{green}green $K_{4}$}.
    But this is not true for $K_{29}$.

    So we say $R(3,3,4) = 30$.

    The proof is done by brute force with \href{https://codingnest.com/modern-sat-solvers-fast-neat-underused-part-1-of-n/}{SAT solver} 🤖️!
\end{frame}

\begin{frame}
    \frametitle{Ramsey's theorem for multiple colours}

    \begin{block}{Theorem 11.6 (Edge-colouring version)}
        Let $r$ and $h_{1}, h_{2}, \ldots, h_{r}$ be positive integers. 
        There exists $R(h_{1}, \ldots, h_{r})$ such that 
        any edge colouring of $K_{R(h_{1}, \ldots, h_{r})}$ with $r$ colours 
        contain a $K_{h_{i}}$ coloured in $i$ for some $i \in [r]$.
    \end{block}

    The \emph{only} exact value of Ramsey number for three or more colours we know are $R(3,
    3, 3) = 17$ and $R(3, 3, 4) = 30$.

    You will get an A (and perhaps any math graduate program 😉️) if you find $R(3,4,4)$!
\end{frame}

\begin{frame}
    \frametitle{Hypergraph}

    A \alert{hypergraph} is a generalization of a 
    graph in which an edge can join any number of vertices.

    A hypergraph in which each edge contains $s$-vertices is called 
    an $s$-uniform hypergraph.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.5\linewidth]{./ramsey-hypergraph.pdf}
        \caption{A $3$-uniform hypergraph}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Ramsey's theorem for hypergraphs}

    \begin{block}{Theorem 11.6 (Hypergraph version)}
        Let $s$ and $h_{1}, h_{2}$ be positive integers. 
        There exists $R(s : h_{1}, h_{2})$ such that 
        any edge colouring of 
        a complete $s$-uniform hypergraph with
        $R(s: h_{1}, h_{2})$ vertices
        with {\color{red}red} and {\color{blue}blue}
        contain 
        \begin{itemize}
            \item either a complete $s$-uniform sub-hypergraph with $h_{1}$ vertices
                coloured in {\color{red}red},
            \item or a complete $s$-uniform sub-hypergraph with $h_{2}$ vertices
                coloured in {\color{blue}blue}.
        \end{itemize}
    \end{block}

    The \emph{only} exact value of Ramsey number for hypergraph is $R(3:4,4) = 13$.
\end{frame}

\begin{frame}
    \frametitle{Ramsey's theorem}

    \begin{block}{Theorem 11.6 (Full version)}
        Let $r$, $s$ and $h_{1}, h_{2}, \ldots, h_{r}$ be positive integers. 
        There exists $R(s : h_{1}, \ldots, h_{r})$ such that 
        any edge colouring of 
        a complete $s$-uniform hypergraph with
        $R(s: h_{1}, \ldots, h_{r})$ vertices
        with $r$ colours 
        contain a complete $s$-uniform sub-hypergraph with $h_{i}$ vertices 
        with only colour $i$ for some $i \in [r]$.
    \end{block}

    The \emph{only} exact value of Ramsey number for hypergraph is $R(3:4,4) = 13$.

    🤔️ What is $R(1:1,1)$, $R(1:1,2)$,$R(1:2,2)$? 
    What is $R(1:h_{1}, h_{2}, \ldots, h_{r})$?
\end{frame}

\begin{frame}[t]
    \frametitle{A final exercise}
    
    Consider $G_{n,p}$ with $p=\frac{1}{2}$.

    Let $X$ be the number of complete subgraphs of size $t= 2 \log_2 n$
    and 
    $Y$ be the number of independent of size $t= 2 \log_2 n$ in $G_{n,p}$.

    Can you show that $E(X+Y) < 1$ when $n$ is large enough?
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Study guide}

    \begin{columns}[c, totalwidth=\textwidth]
        \exercisecolumn{}

        \begin{column}{0.5\textwidth}
            You are recommended to
            \begin{itemize}
                \item 📖 Read
                    \href{https://www.rellek.net/book/app-comb.html}{Applied
                    Combinatorics}, chapter 11.4-11.6
                \item For $R(4,3)$, read this \href{https://www.cut-the-knot.org/arithmetic/combinatorics/Ramsey43.shtml}{webpage}.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
