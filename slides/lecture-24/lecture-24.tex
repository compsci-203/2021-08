%\documentclass[draft]{beamer}
\documentclass[c]{beamer}

\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 24 --- Minimal Spanning Trees}

\begin{document}

\maketitle

\begin{frame}
    \frametitle{Connecting Data Centres}
    What is the minimal cost to connect all data centres?

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.7\linewidth]{./spanning-graph.pdf}
        \caption{Data centres and the cost to connect them}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Two spanning trees}

    There are $16968$ spanning trees for this graph. 
    Each of them represents a way to connect the data centres.
    Which of them cost the least?  

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.45\linewidth]{./spanning-tree-1.pdf}
        \includegraphics[width=0.45\linewidth]{./spanning-tree-2.pdf}
        \caption{What spanning tree costs less?}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Minimal spanning trees}
    
    Given a graph $G = (V, E)$, let $w:E \mapsto \{0,1,2,\dots\}$ be the weights of
    edges.

    The \alert{minimal spanning tree} of $G$ is the spanning tree which has minimizes
    total weight of its edges.

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{./spanning-krushal-4.pdf}
        \caption{A minimal spanning tree}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Spanning forests}
    
    \begin{block}{Proposition 12.3}
        Let $G$ be a graph on $n$ vertices, 
        and let $H = (V, S)$ be a spanning forest. 
        Then $0 \le \abs{S} \le n − 1$.
        Furthermore, if $\abs{S} = n − k$, then $H$ has $k$ components. 
        In particular, $H$ is a spanning tree if and only if it contains $n − 1$ edges.
    \end{block}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.33\linewidth]{./spanning-forest-1.pdf}
        \includegraphics[width=0.33\linewidth]{./spanning-forest-2.pdf}
        \includegraphics[width=0.33\linewidth]{./spanning-forest-3.pdf}
        \caption{Spanning forests}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exchange Principle}

    The following is a spanning tree with an extra edge $e$.

    Note that replacing $f$ with $e$, we still have a spanning tree.
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./path.png}
    \end{figure} 
\end{frame}

\begin{frame}[t]
    \frametitle{Exchange Principle}

    \begin{figure}[htpb]
        \centering
        \includegraphics[width=\linewidth]{./exchange-principle.png}
    \end{figure} 
\end{frame}

\begin{frame}
    \frametitle{Where is the minimal spanning tree?}

    \begin{block}{Lemma 12.6 (AC)}
    Let $F$ be a spanning forest and let $C$ be a component of $F$. Also, let $e = x
    y$ be an edge of minimum weight among all edges with one endpoint in $C$ and the
    other not in $C$.
    Then among all spanning trees that contain the forest $F$, 
    there is one of minimum weight that contains the edge $e$.
    \end{block}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.6\linewidth]{./spanning-forest-4.pdf}
        \caption{Which one is $e$?}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Kruskal's Algorithm -- Avoid Cycles}
    
    \begin{figure}[htpb]
        \centering
        \mbox{%
        \includegraphics<1>[width=0.8\linewidth]{./spanning-krushal-1.pdf}%
        \includegraphics<2>[width=0.8\linewidth]{./spanning-krushal-2.pdf}%
        \includegraphics<3>[width=0.8\linewidth]{./spanning-krushal-3.pdf}}
        \caption{Kruskal's Algorithm by Example}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Try it yourself}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./spanning-exercise.pdf}
        \caption{Can you find a minimal spanning tree?}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Kruskal's Algorithm}

    \begin{block}{Algorithm 12.8}
        Start with a spanning forest $T = (V, S)$ where $S = \emptyset$ and $i = 0$.

        While $|S| < n − 1$, let $j$ be the least non-negative integer so
        that $j > i$ and there are no cycles in $S \cup \{e_j \}$. Then set
        \begin{equation*}
            i = j \qquad \text{and} \qquad S = S \cup \{e_j\}.
        \end{equation*}
    \end{block}

    Prove by induction that at any step, $S$ is contained in a minimal spanning tree.
\end{frame}

\begin{frame}
    \frametitle{Prim's Algorithm}
    
    \begin{block}{Algorithm 12.10}
        Let $r$ be any vertex.  Set $W = \{r\}$ and $S = \emptyset$.
        
        While $|W | < n$, let $e$ be an edge of minimum weight among all edges with
        one endpoint in $W$ and the other not in $W$. If $e = x y$, $x \in W$ and $y \notin
        W$, let
        \begin{equation*}
            W = W \cup \{ y\} \qquad \text{and} \qquad S = S \cup \{e\}.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Try it yourself}
    
    \begin{figure}[htpb]
        \centering
        \includegraphics[width=0.8\linewidth]{./spanning-exercise.pdf}
        \caption{Can you find a minimal spanning tree using Prim's Algorithm?}%
    \end{figure}
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Study guide}

    \begin{columns}[c, totalwidth=\textwidth]
        \exercisecolumn{}

        \begin{column}{0.5\textwidth}
            You are recommended to
            \begin{itemize}
                \item 📖 Read
                    \href{https://www.rellek.net/book/app-comb.html}{Applied
                    Combinatorics}, chapter 12.1
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
