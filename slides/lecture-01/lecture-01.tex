%\documentclass[draft]{beamer}
\documentclass{beamer}

\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 01 -- Mathematical Statements (1)}

\begin{document}

\maketitle

\section{Atomic and Molecular Statements}

\begin{frame}
    \frametitle{Atomic and Molecular Statements}

    A \alert{statement} is any declarative sentence which is either true or false. 

    A statement is \alert{atomic} if it cannot be divided into smaller statements,
    otherwise it is called \alert{molecular}.
\end{frame}

\begin{frame}
    \frametitle{Examples of statements}
    
    These are \alert{atomic} statements
    \begin{itemize}
        \item Telephone numbers in the USA have 10 digits.
        \item 42 is a perfect square.
        \item The 🌙️ is made of 🧀.
        \item Every even number greater than 2 can be expressed as the sum of two primes.
        \item 3+7=12
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Examples of non-statements}
    
    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.5\textwidth}
            
            These are \alert{not} statements
            \begin{itemize}
                \item Would you like some cake?
                \item The sum of two squares.
                \item $1+3+5+7+ \cdots +2n+1$.
                \item Go to the moon!
                \item $3+x=12$.
            \end{itemize}

        \end{column}
        \begin{column}{0.5\textwidth}

            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.5\linewidth]{./cake.jpg}
                \caption*{Photo from Pexels}%
            \end{figure}
            
        \end{column}
    \end{columns}

    🤔️ Can you think of some statements?

    \poll{}
\end{frame}

\begin{frame}
    \frametitle{Molecular statements}
    
    You can build more molecular statements out of simpler ones using \alert{logical
    connectives}.

    \alert{Binary connectives}

    \begin{itemize}
        \item Sam is a 👨️ \alert{and} Chris is a 👩️.
        \item Sam is a 👨️ \alert{or} Chris is a 👩️.
        \item \alert{If} Sam is a 👨️, \alert{then} Chris is a 👩️.
        \item Sam is a 👨️ \alert{if and only if} Chris is a 👩️.
    \end{itemize}

    \pause{}

    \alert{Unnary connective} -- not
    \begin{itemize}
        \item Sam is \alert{not} a 👨️. (Maybe Sam is a 🐕️.)
    \end{itemize}

    🤔️ Can you think of some \alert{molecular} statements?

    \poll{}
\end{frame}

\begin{frame}
    \frametitle{Logical connectives}
    
    \begin{center}
    \begin{tabular}{ c  c  c }
        \toprule
    Name & Notations & Read \\
    \midrule
    Conjunction & $P \wedge Q$ & ``P and Q''\\
    Disjunction & $P \vee Q$ & ``P or Q''\\
    Implication & $P \imp Q$ & ``if P then Q''\\
    Biconditional & $P \iff Q$ & ``P if and only if Q''\\
    Negation & $\neg P$ & ``not P'' \\
    \bottomrule
    \end{tabular}
    \end{center}

    $P$ and $Q$ are called \alert{propositional variables}.
\end{frame}

\begin{frame}
    \frametitle{Truth Conditions for Connectives}
    
    \begin{center}
    \begin{tabular}{ c  l }
        \toprule
    Statement & When is true? \\
    \midrule
    $P \wedge Q$ & $P=✅️$ and $Q=✅️$\\
    $P \vee Q$ & $P=✅️$ or $Q=✅️$\\
    $P \imp Q$ &  $P=❎️$ or $Q=✅️$ or both\\
    $P \iff Q$ & $P=Q=✅️$, or  $P=Q=❎️$\\
    $\neg P$ & $P=❎️$  \\
    \bottomrule
    \end{tabular}
    \end{center}
\end{frame}

\section{Implications}

\begin{frame}
    \frametitle{Implications}

    An \alert{implication} is a molecular statement of the form $P \imp Q$.

    We say that
    \begin{itemize}
        \item $P$ is the \alert{hypothesis} (not to confused with \emph{hypnosis} 😵️).
        \item $Q$ is the \alert{conclusion}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Mathematics statements}

    Most statements in mathematics are implications.

    \begin{columns}[totalwidth=\textwidth]
        \begin{column}{0.6\textwidth}

            \begin{block}{Pythagorean Theorem/勾股定理}
                If $a$ and $b$ are the legs of a right triangle with hypotenuse $c$, 
                then
                \begin{equation*}
                    a^2 + b^2 = c^2.
                \end{equation*}
            \end{block}

        \end{column}
        \begin{column}{0.4\textwidth}

            \begin{figure}
                \centering
                \includegraphics[width=0.7\textwidth]{./Pythagorean.png}
                \caption*{From
                \href{https://commons.wikimedia.org/w/index.php?curid=640875}{Wikipedia}}
            \end{figure}

        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[t]
    \frametitle{True or false?}
    When is $P \imp Q = ✅️$?
        \begin{itemize}
            \item $P= Q = ✅️$, or
            \item $P=❎️$.
        \end{itemize}

    When is $P \imp Q =❎️$?
    \begin{itemize}
        \item $P= ✅️$ and $Q = ❎️$.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{When am I lying?}
    
    Assume that I told Bob \emph{If you get a 90 on the final, then you will pass the
    class.}
    In which case can Bob call me a liar 😥️?

    \vspace{8em}

    💣️ ``P implies Q'' does \alert{not} mean $P$ causes or is the reason of $Q$.
\end{frame}

\begin{frame}[label=current]
    \frametitle{Exercise}

    Which of the following statements are true?
    \begin{enumerate}
        \item If $1=1$, then most horses have 4 legs.
        \item If $0=1$, then $1=1$.
        \item If $8$ is a prime number, then the 7624th digit of $\pi$ is an $8$.
        \item If the 26th digit of $\pi$ is an $3$, then $2+2=5$.
    \end{enumerate}

    \practice{}
\end{frame}

\begin{frame}[t]
    \frametitle{Direct proof of implications}
    
    To prove an implication $P \imp Q$, it is enough to \emph{assume} $P$, and from
    it, deduce $Q$.
    
    \begin{exampleblock}{Example}
        Prove that if two numbers $a$ and $b$ are even, then their sum $a + b$ is even.
    \end{exampleblock}
\end{frame}

\section{Converses and Contrapositives}

\begin{frame}
    \frametitle{Converses}

    The \alert{converse} of $P \imp Q$ is $Q \imp P$.

    The converse \alert{$\ne$} the original implication.

    \begin{exampleblock}{Example}
        Implication: If an integer is greater than 2 is prime, then that number is odd.

        Converse: If an integer is odd, then it is a prime number greater than 2.

        Are these statement true?
    \end{exampleblock}
\end{frame}

\begin{frame}[t]
    \frametitle{Contrapositives}

    The contrapositive of $P \imp Q$ is the statement $\neg Q \imp \neg P$. 

    An implication \alert{$=$} its contrapositive.

    \begin{exampleblock}{Example}
        True or false: If you draw at least nine playing cards from a deck,
        then you will have at least three cards all of the same suit. 

        Is the \alert{converse} true?
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Susan's grade}
    
    Suppose that \emph{if Susan gets a 93\% on her final, then she will get an A in
    the class}.

    What can you conclude in the following cases:

    \begin{itemize}
        \item Susan gets a 93\% on her final.
        \item Susan gets an A in the class.
        \item Susan does not get a 93\% on her final.
        \item Susan does not get an A in the class.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Exercise}

    Consider \emph{If you will give me a 🐼, then I will give you 🦄.}

    Are the statements below its \emph{converse}, \emph{contrapositive} or
    \emph{neither}?

    \begin{enumerate}
        \small
        \item If you will give me a 🐼, then I will not give you 🦄.
        \item If I will not give you 🦄, then you will not give me a 🐼.
        \item If I will give you 🦄, then you will give me a 🐼.
        \item If you will not give me a 🐼, then I will not give you 🦄.
        \item You will give me a 🐼 and I will not give you 🦄.
    \end{enumerate}

    \practice{}
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Study guide}

    \begin{columns}[totalwidth=\textwidth]
        \exercisecolumn{}
        \begin{column}{0.5\textwidth}
            You are \alert{recommended} (not required) to
            \begin{itemize}
                \item 📖 Read \href{http://discrete.openmathbooks.org/dmoi3/sec_intro-statements.html}{Discrete Mathematics}, page 1-9.
                \item 🖊 Solve
                    \href{http://discrete.openmathbooks.org/dmoi3/sec_intro-statements.html}{Discrete
                    Mathematics}, chapter 0.2: exercise 1, 3, 5, 6, 9.
            \end{itemize}
        \end{column}
    \end{columns}

\end{frame}

\end{document}
