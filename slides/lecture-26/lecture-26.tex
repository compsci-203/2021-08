%\documentclass[draft]{beamer}
\documentclass[c]{beamer}

\input{../meta.tex}

%\includeonlyframes{current}

\title{Lecture 26 --- Probability (2)}

\begin{document}

\maketitle

\section{Central Tendency}

\begin{frame}[t]
    \frametitle{Markov's inequality}

    \begin{block}{Theorem 10.20 (AC)}
        Let $X$ be a random variable. Then for any $k > 0$,
        \begin{equation*}
            P(\abs{X} \ge k) \le \frac{E(\abs{X})}{k}.
        \end{equation*}
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{The variance}
    
    The quantity 
    \begin{equation*}
        \Var(X) 
        =
        E((X-E(X))^{2})
        = 
        E(X^{2})-E(X)^{2}  
    \end{equation*}
    is called the \alert{variance} of
    $X$.

    🤔️ Assume $X = 1$ with probability $p$ and $X=0$ with probability $0$.
    What is $\Var(X)$?
\end{frame}

\begin{frame}
    \frametitle{The variance of Bernoulli trials}

    \begin{exampleblock}{Bernoulli trials}
        In a Bernoulli trails with $n$ experiments, the number of success $X$ has
        $E(X) = n p$.
        \begin{equation*}
        \Var(X) = 
        \sum_{i=0}^{n}
        (i- n p)^{2}
        \binom{n}{i} p^{i} (1-p)^{n-i}
        =
        n p (1-p)
        .
        \end{equation*}
    \end{exampleblock}

    We can get this by using
    \begin{equation*}
        \Var(X_{1}+X_{2}+\cdots+X_{n})
        =
        \Var(X_{1})+\Var(X_{2})+\cdots+\Var(X_{n})
        ,
    \end{equation*}
    when $X_{1}, \ldots, X_{n}$ are 
    \href{https://mathworld.wolfram.com/IndependentVariable.html}{independent}.
\end{frame}

\begin{frame}[t]
    \frametitle{Chebyshev's Inequality}
    
    \begin{block}{Theorem 10.24 (AC)}
        Let $X$ be a random variable. Then for all $k>0$,
        \begin{equation*}
            P\left(\abs{X - E(X)} > k \sqrt{\Var(X)}\right) \le \frac{1}{k^{2}}.
        \end{equation*}
    \end{block}

    🤔️ Can you get an upper bound of the probability of getting at most $2500$ or
    at least $7500$ heads in $10000$ tosses of a fair coin?
\end{frame}

\section{Random Graphs}

\begin{frame}
    \frametitle{Paul Erdős}

    \begin{columns}
        \begin{column}{0.5\textwidth}
        Paul Erdős (1913--1996) was one of the most prolific mathematicians in history.
        \begin{itemize}
            \item 1,525 mathematical articles
            \item 511 different collaborators
            \item Great ``problem solver''
            \item Ramsey theory, probabilistic method, random graphs 
                \ldots
        \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}[htpb]
                \centering
                \includegraphics[width=0.8\linewidth]{./Erdos.jpg}
                \caption{From
                    \href{https://en.wikipedia.org/wiki/Paul_Erd\%C5\%91s}{Wikipedia}}%
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Erdős-Rényi model}
    
    $G_{n,m}$ is a (simple) graph chosen uniformly at random from all (simple)
    graphs with $n$ vertices and $m$ edges.

    \begin{figure}[htpb]
        \centering
        \only<1>{\includegraphics[width=0.4\linewidth]{./gnm-1.pdf}}%
        \only<2>{\includegraphics[width=0.4\linewidth]{./gnm-2.pdf}}%
        \only<3>{\includegraphics[width=0.4\linewidth]{./gnm-3.pdf}}
        \caption{Examples of $G_{10,15}$}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The binomial random graph model}
    
    $G_{n,p}$ is a random (simple) graph with $n$ vertices in which between each pair
    of vertices there is an edge present with probability $p$.

    🤔️ The expected number of edges in $G_{n,p}$ is ❓️

    \begin{figure}[htpb]
        \centering
        \only<1>{\includegraphics[width=0.4\linewidth]{./gnm-1.pdf}}%
        \only<2>{\includegraphics[width=0.4\linewidth]{./gnp-1.pdf}}%
        \only<3>{\includegraphics[width=0.4\linewidth]{./gnp-2.pdf}}
        \caption{Examples of $G_{10,1/2}$}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Isolated vertices}
    \begin{figure}[htpb]
        \centering
        \only<1>{\includegraphics[width=0.8\linewidth]{./gnp-sub.pdf}}%
        \only<2>{\includegraphics[width=0.8\linewidth]{./gnp-super.pdf}}%
        \caption{Experiments with $100$ $G_{1000,p}$}%
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{The phase transition phenomenon}

    \begin{theorem}
        (i) The probability of that $G_{n,p}$ is connected goes to $1$ if 
        $p > c \frac{\log n}{n}$ for some constant $c > 1$.

        (ii) The probability of that $G_{n,p}$ is connected goes to $0$ if 
        $p < c \frac{\log n}{n}$ for some constant $c < 1$.
    \end{theorem}

    \begin{figure}[htpb]
        \centering
        \only<1>{\includegraphics[width=0.3\linewidth]{./gnp-large-1.pdf}}%
        \only<2>{\includegraphics[width=0.4\linewidth]{./gnp-large-2.pdf}}%
        \caption{When \only<1>{$p = 0.9 \log n/n$}\only<2>{$p = 1.1 \log n/n$}}%
    \end{figure}
\end{frame}

\begin{frame}[t]
    \frametitle{First-moment method}
    \begin{theorem}
        (i) The probability of that $G_{n,p}$ has isolated vertices goes to $1$ if 
        $p = c \frac{\log n}{n}$ for some constant $c < 1$.

        (ii) The probability of that $G_{n,p}$ has isolated vertices goes to $0$ if 
        $p = c \frac{\log n}{n}$ for some constant $c > 1$.
    \end{theorem}

    For (ii) we use \alert{first-moment method}. 

    Let $X$ be the number of isolated vertices in
    $G_{n,p}$.  By Markov's inequality,
    \begin{equation*}
        P(X \ge 1) \le \frac{E(X)}{1} = E(X).
    \end{equation*}
    So it suffices to show that $E(X) \to 0$ as $n \to \infty$.
\end{frame}

\begin{frame}
    \frametitle{Second-moment method}

    For (i) we use \alert{second-moment method}. 

    By Chebyshev's inequality
    \begin{align*}
        P(X \le 0) 
        &
        =
        P(X - E(X) \le - E(X))
        \\
        &
        \le
        P(\abs{X - E(X)} \ge E(X))
        \\
        &
        =
        P(\abs{X - E(X)} \ge \frac{E(X)}{\sqrt{\Var(X)}} \sqrt{\Var(X)})
        \\
        &
        \le
        \frac{\Var{X}}{E(X)^{2}}
        =
        \frac{E(X^{2})-E(X)^{2}}{E(X)^{2}}
        =
        \frac{E(X^{2})}{E(X)^{2}}
        -1
        .
    \end{align*}
    So it suffices to show that $\frac{E(X^{2})}{E(X)^{2}} \to 1$ as $n \to \infty$.
\end{frame}

\begin{frame}[t]
    \frametitle{Paths of length $2$}
    
    🤔️ Let $p = n^{-3/2}/\log n$. Can you prove that the probability $G_{n,p}$ contains
    a path of length at least $2$ goes to $0$?

    Hint: Let $X$ be the number of paths of length $2$. What is $E(X)$? What is
    $\lim_{n \to \infty} E(X)$?
\end{frame}

\appendix{}

\begin{frame}
    \frametitle{Study guide}

    \begin{columns}[c, totalwidth=\textwidth]
        \exercisecolumn{}

        \begin{column}{0.5\textwidth}
            You are recommended to
            \begin{itemize}
                \item 📖 For first and second moment method, read \href{https://www.math.cmu.edu/~af1p/BOOK.pdf}{Introduction to Random Graphs}, Section 1.2.
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\end{document}
